package tpbuild.com.tpbuild;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpbuildApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpbuildApplication.class, args);
	}

}
